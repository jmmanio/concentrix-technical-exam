from django.urls import path

from users.views import UserListView

app_name = 'users'

urlpatterns = [
    path('<str:user>', UserListView.as_view(), name='details'),
]
