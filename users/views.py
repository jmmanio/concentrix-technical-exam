from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.db.models import F
from django.db.models import Max
from django.views.generic import CreateView

from core.views import JokeListViewWithCategory
from jokes.models import Joke
from users.models import UserJokeability


class UserListView(JokeListViewWithCategory):
    template_name = 'users/user_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['selected_user'] = self.kwargs.get('user')
        return context

    def get_queryset(self):
        user = self.kwargs.get('user')
        return Joke.objects.filter(sender__username=user).order_by('-created_date')


class UserJokeabilityView(JokeListViewWithCategory):
    template_name = 'users/jokeability_list.html'
    paginate_by = 10

    def get_queryset(self):
        return UserJokeability.objects.annotate(
            max_date=Max('joker__userjokeability__created_date')).filter(
            created_date=F('max_date'), jokeability__gte=0.85).order_by(
            '-jokeability', '-thumbs_up', '-created_date')


class UserLogin(LoginView):
    template_name = 'users/login.html'


class UserLogout(LogoutView):
    pass


class UserRegister(CreateView):
    template_name = 'users/register.html'
    form_class = UserCreationForm
    success_url = '/login'


class SubmitJoke(LoginRequiredMixin, CreateView):
    template_name = 'users/submit_joke.html'
    model = Joke
    fields = ('category', 'title', 'joke',)
    success_url = '/'

    def form_valid(self, form):
        form.instance.sender = self.request.user
        return super(SubmitJoke, self).form_valid(form)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SubmitJoke, self).get_context_data(**kwargs)
        context['categories'] = [choices[1] for choices in Joke.CATEGORY_CHOICES]
        return context
