from django.contrib.auth.models import User
from django.db import models

from core.models import AuditTrailAbstractModel


class UserJokeability(AuditTrailAbstractModel):
    joker = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Joker')
    jokeability = models.FloatField(default=0.0, verbose_name='Jokeability')
    thumbs_up = models.IntegerField(default=0, verbose_name='Total Thumbs Up')
    thumbs_down = models.IntegerField(default=0, verbose_name='Total Thumbs Down')

    class Meta:
        db_table = 'user_jokeability'
        ordering = ('-created_date',)
        verbose_name = 'User Jokeability'
        verbose_name_plural = 'User Jokeability'

    def __str__(self):
        return '{} - {}'.format(str(self.joker), str(self.jokeability))
