Full Stack Developer Exam
=========================

Submission for Concentrix job application


Installation
------------


1. Install requirements thru ``pip``:

.. code-block:: console

    pip install -r requirements.txt


2. Create new file ``secret_key.txt`` with code generated from https://miniwebtool.com/django-secret-key-generator/.


3. Migrate database models:

.. code-block:: console

    python manage.py makemigrations --settings=jokeability.settings.local
    python manage.py migrate --settings=jokeability.settings.local


Usage
-----

1. Run Django server:

.. code-block:: console

    python manage.py runserver --settings=jokeability.settings.local


2. Open the website on http://127.0.0.1:8000/:

.. image:: screenshots/index.png


3. Register an account on http://127.0.0.1:8000/register:

.. image:: screenshots/register.png


4. Login on http://127.0.0.1:8000/login:

.. image:: screenshots/login.png


Pages
-----

List of pages available to the user:

.. image:: screenshots/pages.png

- Index page: Lists all the jokes sent
- Categories: Lists all the jokes based on the category selected. Available joke categories are: ``Irony``, ``Character``, ``Reference``, ``Shock``, ``Parody``, ``Hyperbole``, ``Wordplay``, ``Analogy``, ``Madcap``, ``Meta-humor``, and ``Misplaced-focus``.
- Top Jokers: Lists all of the users with "Jokeability" of 85% or more.
- <user>: Joke Profile: Lists all of the jokes sent by the current user
- Submit Joke: Form to submit a joke
- Sign Out: Log out of the current session
