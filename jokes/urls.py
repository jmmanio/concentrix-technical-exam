from django.urls import path

from jokes.views import CategoryListView
from jokes.views import RatingView

app_name = 'jokes'

urlpatterns = [
    path('categories/<str:category>', CategoryListView.as_view(), name='categories'),
    path('rating', RatingView.as_view(), name='rating'),
]
