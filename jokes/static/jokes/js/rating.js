$(function () {
    'use strict';

    const RATING_URL = '/jokes/rating';

    let $csrf_token = $('input[name="csrfmiddlewaretoken"]');
    let $ratings = $('.rating');
    let $thumbs_up = $('.thumbs-up');
    let $thumbs_down = $('.thumbs-down');

    function get_pk($elem) {

        return $elem.closest('p').find('input:hidden').val();
    }

    function save_rating($elem, pk, rating) {

        let last_count = parseInt($elem.find('.count').html());

        if ($elem.hasClass('active')) {
            $.post(RATING_URL, {
                'pk': pk,
                'rating': null,
                'csrfmiddlewaretoken': $csrf_token.val()
            }, function () {
                $elem.removeClass('active');

                // Deduct value to previous button
                $elem.find('.count').html(--last_count);
            });
        }
        else {
            $.post(RATING_URL, {
                'pk': pk,
                'rating': rating,
                'csrfmiddlewaretoken': $csrf_token.val()
            }, function () {
                $elem.addClass('active');

                // Add count to button clicked
                $elem.find('.count').html(++last_count);

                // Deduct count from sibling button
                let $sibling = $elem.siblings('button').eq(0);

                if ($sibling.hasClass('active')) {
                    let sibling_count = parseInt($sibling.find('.count').html());
                    $sibling.removeClass('active');
                    $sibling.find('.count').html(--sibling_count);
                }
            });
        }
    }

    $thumbs_up.on('click', function () {

        let pk = get_pk($(this));
        save_rating($(this), pk, true);
    });

    $thumbs_down.on('click', function () {

        let pk = get_pk($(this));
        save_rating($(this), pk, false);
    });


    // Loop thru each rating and update count
    $ratings.each(function () {

        let $elem = $(this);
        let pk = get_pk($elem);

        $.get(RATING_URL, {'pk': pk}, function (data) {

            let $thumbs_up = $elem.closest('p').find('.thumbs-up');
            let $thumbs_down = $elem.closest('p').find('.thumbs-down');
            let $thumbs_up_count = $thumbs_up.find('.count');
            let $thumbs_down_count = $thumbs_down.find('.count');

            $thumbs_up_count.html(data.up);
            $thumbs_down_count.html(data.down);

            if (data.rating === true) {
                $thumbs_up.addClass('active');
            }
            else if (data.rating === false) {
                $thumbs_down.addClass('active');
            }
        });
    });
});
