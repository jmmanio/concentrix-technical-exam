from django.views.generic import View
from django.http.response import JsonResponse

from core.views import JokeListViewWithCategory
from jokes.models import Joke
from jokes.models import JokeRating
from jokes.models import JokeRatingCount
from users.models import UserJokeability


class JokeListView(JokeListViewWithCategory):
    pass


class CategoryListView(JokeListViewWithCategory):
    template_name = 'jokes/category_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['category'] = self.kwargs.get('category').capitalize()
        return context

    def get_queryset(self):
        category = self.kwargs.get('category')
        category_choice = ''

        # Get category value based on category text URL slug received
        for cat in Joke.CATEGORY_CHOICES:
            value = cat[0]
            text = cat[1].lower()
            if text == category:
                category_choice = value
                break

        return Joke.objects.filter(category__contains=category_choice).order_by('-created_date')


class RatingView(View):

    def get(self, *args, **kwargs):
        user = self.request.user
        pk = self.request.GET.get('pk', None)

        # Get last rating
        try:
            joke_rating = JokeRating.objects.filter(joke_id=pk, critic=user).first()
            last_rating = joke_rating.rating
        except (AttributeError, TypeError):
            last_rating = None

        # Get last count
        rating_count = JokeRatingCount.objects.filter(joke_id=pk).first()

        try:
            # Get last joke rating count
            data = {'up': rating_count.thumbs_up, 'down': rating_count.thumbs_down, }
        except AttributeError:
            data = {'up': 0, 'down': 0, }
        finally:
            data['rating'] = last_rating
            return JsonResponse(data)

    def post(self, *args, **kwargs):
        user = self.request.user
        pk = self.request.POST.get('pk', None)
        rating = self.request.POST.get('rating', None)

        # Get previous rating and count
        prev_rating = JokeRating.objects.filter(joke_id=pk, critic=user).first()
        prev_count = JokeRatingCount.objects.filter(joke_id=pk).first()

        # Prepare new count
        try:
            new_count = JokeRatingCount(
                joke_id=pk, thumbs_up=prev_count.thumbs_up, thumbs_down=prev_count.thumbs_down
            )
        except AttributeError:
            new_count = JokeRatingCount(joke_id=pk)

        #
        #   Update rating
        #
        if rating == 'true':
            rating = True
            new_count.thumbs_up += 1
        elif rating == 'false':
            rating = False
            new_count.thumbs_down += 1
        else:
            # If new rating is 'None', base the deduction on previous rating
            rating = None
            if prev_rating.rating:
                new_count.thumbs_up -= 1
            else:
                new_count.thumbs_down -= 1

        #
        #   If rating is switched, reduce previous position count
        #
        try:
            if rating and prev_rating.rating is False:
                new_count.thumbs_down = abs(new_count.thumbs_down - 1)
            elif rating is False and prev_rating:
                new_count.thumbs_up = abs(new_count.thumbs_up - 1)
        except AttributeError:
            pass

        #
        #   Update user jokeability rating
        #
        prev_jokeability = UserJokeability.objects.filter(joker_id=new_count.joke.sender_id).first()
        new_jokeability = UserJokeability(joker_id=new_count.joke.sender_id)
        try:
            # Try to get new jokability values based on previous and current counts
            new_jokeability.thumbs_up = abs(prev_jokeability.thumbs_up + (
                    new_count.thumbs_up - prev_count.thumbs_up))
            new_jokeability.thumbs_down = abs(prev_jokeability.thumbs_down + (
                    new_count.thumbs_down - prev_count.thumbs_down))
        except AttributeError:
            try:
                # Else, get new jokabiltiy values based on previous jokability values and new count
                new_jokeability.thumbs_up = prev_jokeability.thumbs_up + new_count.thumbs_up
                new_jokeability.thumbs_down = prev_jokeability.thumbs_down + new_count.thumbs_down
            except AttributeError:
                # Else, new jokability will be the values of the new count
                new_jokeability.thumbs_up = new_count.thumbs_up
                new_jokeability.thumbs_down = new_count.thumbs_down
        finally:
            # Now, compute jokability percent
            try:
                new_jokeability.jokeability = round(
                        new_jokeability.thumbs_up / (new_jokeability.thumbs_up + new_jokeability.thumbs_down), 4)
            except ZeroDivisionError:
                new_jokeability.jokeability = 0.0

        # Save new rating
        new_rating = JokeRating(critic=user, joke_id=pk, rating=rating)
        new_rating.save()

        # Save new count
        new_count.save()

        # Save new jokeability
        new_jokeability.save()

        return JsonResponse(data={'up': new_count.thumbs_up, 'down': new_count.thumbs_down, })
