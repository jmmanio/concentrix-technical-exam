from django.contrib.auth.models import User
from django.db import models

from multiselectfield import MultiSelectField

from core.models import AuditTrailAbstractModel
from core.models import IsActiveAbstractModel


class Joke(AuditTrailAbstractModel, IsActiveAbstractModel):
    # Reference
    #   Every Joke Falls in One of 11 Categories, Says Founding Editor of The Onion
    #   https://bigthink.com/stephen-johnson/every-joke-falls-in-one-of-these-11-categories-according-to-the-founder-of-the-onion
    CATEGORY_CHOICES = (
        ('IR', 'Irony'),
        ('CH', 'Character'),
        ('RE', 'Reference'),
        ('SH', 'Shock'),
        ('PA', 'Parody'),
        ('HY', 'Hyperbole'),
        ('WO', 'Wordplay'),
        ('AN', 'Analogy'),
        ('MA', 'Madcap'),
        ('ME', 'Meta-humor'),
        ('MI', 'Misplaced-focus'),
    )

    sender = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Sender')
    category = MultiSelectField(choices=CATEGORY_CHOICES, verbose_name='Category')
    title = models.CharField(max_length=128, verbose_name='Title')
    joke = models.TextField(verbose_name='Joke')

    class Meta:
        db_table = 'joke'
        verbose_name = 'Joke'
        verbose_name_plural = 'Jokes'
        ordering = ('-created_date',)

    def __str__(self):
        return self.title


class JokeRating(AuditTrailAbstractModel):
    critic = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Critic')
    joke = models.ForeignKey(Joke, on_delete=models.CASCADE, verbose_name='Joke')
    rating = models.NullBooleanField(verbose_name='Rating')

    class Meta:
        db_table = 'joke_rating'
        verbose_name = 'Joke Rating'
        verbose_name_plural = 'Joke Ratings'
        ordering = ('-created_date',)

    def __str__(self):
        return '{} - {} - {}'.format(self.critic, str(self.joke), str(self.rating))


class JokeRatingCount(AuditTrailAbstractModel):
    joke = models.ForeignKey(Joke, on_delete=models.CASCADE, verbose_name='Joke')
    thumbs_up = models.IntegerField(default=0)
    thumbs_down = models.IntegerField(default=0)

    class Meta:
        db_table = 'joke_rating_count'
        verbose_name = 'Joke Rating Count'
        verbose_name_plural = 'Joke Rating Count'
        ordering = ('-created_date',)

    def __str__(self):
        return '{} - {} - {}'.format(str(self.joke), str(self.thumbs_up), str(self.thumbs_down))
