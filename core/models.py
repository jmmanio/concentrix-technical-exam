from django.db import models


class AuditTrailAbstractModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True, editable=False)
    modified_date = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class IsActiveAbstractModel(models.Model):
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True
