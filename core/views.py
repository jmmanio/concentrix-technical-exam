from django.views.generic import ListView

from jokes.models import Joke


class JokeListViewWithCategory(ListView):
    paginate_by = 10
    model = Joke

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(JokeListViewWithCategory, self).get_context_data(**kwargs)
        context['categories'] = [choices[1] for choices in Joke.CATEGORY_CHOICES]
        return context
