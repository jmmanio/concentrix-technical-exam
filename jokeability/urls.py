"""jokeability URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include
from django.urls import path

from jokes.views import JokeListView
from users.views import SubmitJoke
from users.views import UserJokeabilityView
from users.views import UserLogin
from users.views import UserLogout
from users.views import UserRegister

urlpatterns = [
    path('login', UserLogin.as_view(), name='login'),
    path('logout', UserLogout.as_view(), name='logout'),
    path('register', UserRegister.as_view(), name='register'),
    path('submit-joke', SubmitJoke.as_view(), name='submit_joke'),

    path('jokes/', include('jokes.urls')),
    path('users/', include('users.urls')),
    path('top-jokers', UserJokeabilityView.as_view(), name='top_jokers'),
    path('', JokeListView.as_view(), name='index'),
]
