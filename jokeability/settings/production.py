from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']

INTERNAL_IPS = []

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
